var path = require("path");
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var FileManagerPlugin = require("filemanager-webpack-plugin");

module.exports = {
  entry: ["babel-polyfill", "whatwg-fetch", "./src/index.js"],

  output: {
    path: path.join(__dirname, "./public"),
    filename: "app.js"
  },

  resolve: {
    modulesDirectories: ["node_modules", "src"]
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ["babel"],
        include: path.join(__dirname, "src"),
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract(
          "style-loader",
          "css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader"
        )
      }
    ]
  },

  postcss: [require("autoprefixer"), require("postcss-modules-values")],

  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production")
      }
    }),
    new ExtractTextPlugin("style.css", { allChunks: true }),
    new FileManagerPlugin({
      onEnd: {
        copy: [
          { source: "./static/*.{html,css,js,png}", destination: "./public" }
        ]
      }
    })
  ]
};
