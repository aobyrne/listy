import { takeEvery } from "redux-saga";
import { call, put, fork } from "redux-saga/effects";
import { AppleService } from "utils/apple-service";
import { GET_LISTS_REQUEST, setLists } from "actions";
import { ADD_LIST_REQUEST, addList, addListSuccess } from "actions";
import { UPDATE_LIST_REQUEST, updateList, updateListSuccess } from "actions";
import { REMOVE_LIST_REQUEST, removeList, removeListSuccess } from "actions";
import {
  SELECT_LIST_REQUEST,
  selectList,
  toggleLists,
  getItemsRequest
} from "actions";
import { SHARE_LIST_REQUEST } from "actions";

function* getListsSaga(action) {
  // yield call(AppleService.createZone, 'stuff')
  // yield call(AppleService.acceptShare, '0YHcX8B8FDZTAwpvu4P2bbheg')
  const lists = yield call(AppleService.getLists, action.value);
  yield put(setLists(lists));
}

function* addListSaga(action) {
  const { title } = action.value;
  yield put(addList({ id: "temp", title }));
  const list = yield call(AppleService.createList, title);
  yield put(updateList({ id: "temp", updates: { id: list.id } }));
  yield put(addListSuccess());
}

function* removeListSaga(action) {
  yield put(removeList(action.value));
  yield call(AppleService.removeList, action.value);
  yield put(removeListSuccess());
}

function* updateListSaga(action) {
  const { list, updates } = action.value;
  yield put(updateList({ id: list.id, updates: updates }));
  const { recordChangeTag } = yield call(
    AppleService.updateList,
    list,
    updates
  );
  yield put(updateList({ id: list.id, updates: { recordChangeTag } }));
  yield put(updateListSuccess());
}

function* selectListSaga(action) {
  const { id } = action.value;
  yield put(toggleLists(false));
  yield put(selectList(action.value));
  yield put(getItemsRequest(id));
}

function* shareListSaga(action) {
  const response = yield call(AppleService.shareList, action.value);
  yield console.debug("share response", response);
}

export default [
  fork(takeEvery, GET_LISTS_REQUEST, getListsSaga),
  fork(takeEvery, ADD_LIST_REQUEST, addListSaga),
  fork(takeEvery, REMOVE_LIST_REQUEST, removeListSaga),
  fork(takeEvery, SELECT_LIST_REQUEST, selectListSaga),
  fork(takeEvery, SHARE_LIST_REQUEST, shareListSaga),
  fork(takeEvery, UPDATE_LIST_REQUEST, updateListSaga)
];
