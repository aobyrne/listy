import itemSaga from "./item";
import listSaga from "./list";

const sagas = itemSaga.concat(listSaga);

export default function* rootSaga() {
  yield sagas;
}
