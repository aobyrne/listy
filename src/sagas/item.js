import { takeEvery } from "redux-saga";
import { call, put, fork } from "redux-saga/effects";
import { GET_ITEMS_REQUEST, setItems } from "actions";
import { ADD_ITEM_REQUEST, addItem, addItemSuccess } from "actions";
import { UPDATE_ITEM_REQUEST, updateItem, updateItemSuccess } from "actions";
import { REMOVE_ITEM_REQUEST, removeItem, removeItemSuccess } from "actions";
import { AppleService } from "utils/apple-service";

function* getItemsSaga(action) {
  const items = yield call(AppleService.getItems, action.value);
  yield put(setItems(items));
}

function* addItemSaga(action) {
  const { title, listId } = action.value;
  yield put(
    addItem({ id: "temp", title, listId, modified: new Date().getTime() })
  );
  const { id, recordChangeTag } = yield call(
    AppleService.createItem,
    title,
    listId
  );
  yield put(updateItem({ id: "temp", updates: { id, recordChangeTag } }));
  yield put(addItemSuccess());
}

function* updateItemSaga(action) {
  const { item, updates } = action.value;
  yield put(updateItem({ id: item.id, updates: updates }));
  const { recordChangeTag } = yield call(
    AppleService.updateItem,
    item,
    updates
  );
  yield put(updateItem({ id: item.id, updates: { recordChangeTag } }));
  yield put(updateItemSuccess());
}

function* removeItemSaga(action) {
  const id = action.value;
  yield put(removeItem(id));
  yield call(AppleService.removeItem, id);
  yield put(removeItemSuccess());
}

export default [
  fork(takeEvery, GET_ITEMS_REQUEST, getItemsSaga),
  fork(takeEvery, ADD_ITEM_REQUEST, addItemSaga),
  fork(takeEvery, UPDATE_ITEM_REQUEST, updateItemSaga),
  fork(takeEvery, REMOVE_ITEM_REQUEST, removeItemSaga)
];
