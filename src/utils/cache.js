const cache = key => {
  const CACHE_KEY = key;
  return {
    set: value => localStorage.setItem(CACHE_KEY, JSON.stringify(value)),
    get: () => JSON.parse(localStorage.getItem(CACHE_KEY))
  };
};

export default cache;
