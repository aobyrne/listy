const ZONE_NAME = "_defaultZone";

class AppleApi {
  static getOptions() {
    return { zoneID: { zoneName: ZONE_NAME } };
  }

  static create(record) {
    return new Promise((resolve, reject) => {
      const container = CloudKit.getDefaultContainer();
      const db = container.privateCloudDatabase;
      db.saveRecords(record, AppleApi.getOptions()).then(response => {
        if (response.hasErrors) {
          reject(response.errors[0]);
        } else {
          resolve(response.records[0]);
        }
      });
    });
  }

  static update(record) {
    return new Promise((resolve, reject) => {
      const container = CloudKit.getDefaultContainer();
      const db = container.privateCloudDatabase;
      // const query = {
      //   operationType : 'update',
      //   recordType: 'Item',
      //   record: record
      // };
      db.saveRecords(record, AppleApi.getOptions()).then(response => {
        if (response.hasErrors) {
          reject(response.errors[0]);
        } else {
          resolve(response.records[0]);
        }
      });
    });
  }

  static share(record) {
    return new Promise((resolve, reject) => {
      const container = CloudKit.getDefaultContainer();
      const db = container.privateCloudDatabase;
      const payload = {
        atomic: true,
        record: { recordName: record.recordName },
        shareTitle: "zzz",
        zoneID: { zoneName: ZONE_NAME },
        supportedAccess: ["PRIVATE"],
        supportedPermissions: ["READ_WRITE"]
      };
      db.shareWithUI(payload).then(response => {
        if (response.hasErrors) {
          reject(response.errors[0]);
        } else {
          resolve(response);
        }
      });
    });
  }

  static acceptShare(shortGUID) {
    return new Promise((resolve, reject) => {
      const container = CloudKit.getDefaultContainer();
      container.acceptShares(shortGUID).then(response => {
        if (response.hasErrors) {
          reject(response.errors[0]);
        } else {
          console.debug("share accepted", response.results[0]);
          resolve(response.results[0]);
        }
      });
    });
  }

  static createZone(zoneName) {
    return new Promise((resolve, reject) => {
      const container = CloudKit.getDefaultContainer();
      const db = container.privateCloudDatabase;
      db.saveRecordZones({ zoneName }).then(response => {
        if (response.hasErrors) {
          reject(response.errors[0]);
        } else {
          resolve(response.zones[0]);
        }
      });
    });
  }

  static delete(id) {
    return new Promise((resolve, reject) => {
      const container = CloudKit.getDefaultContainer();
      const db = container.privateCloudDatabase;
      db.deleteRecords(id, AppleApi.getOptions()).then(response => {
        if (response.hasErrors) {
          reject(response.errors[0]);
        } else {
          resolve(id);
        }
      });
    });
  }

  static fetch(id) {
    return new Promise((resolve, reject) => {
      const container = CloudKit.getDefaultContainer();
      const db = container.privateCloudDatabase;
      db.fetchRecords(id, AppleApi.getOptions()).then(response => {
        if (response.hasErrors) {
          reject(response.errors[0]);
        } else {
          resolve(response);
        }
      });
    });
  }

  static list(query, options) {
    return new Promise((resolve, reject) => {
      const container = CloudKit.getDefaultContainer();
      const db = container.privateCloudDatabase;
      options = Object.assign({}, options, AppleApi.getOptions());
      db.performQuery(query, options).then(response => {
        if (response.hasErrors) {
          reject(response.errors[0]);
        } else {
          resolve(response.records);
        }
      });
    });
  }

  static subscribe(listId) {
    console.debug("subscribe", listId);
    return new Promise(resolve => {
      const container = CloudKit.getDefaultContainer();
      const db = container.privateCloudDatabase;
      let subscription = {
        subscriptionType: "query",
        firesOn: ["create", "update", "delete"],
        query: {
          recordType: "Item",
          filterBy: [
            {
              fieldName: "list",
              comparator: "EQUALS",
              fieldValue: {
                value: {
                  recordName: listId
                }
              }
            }
          ]
        }
      };
      db.saveSubscriptions(subscription).then(response => {
        if (response.hasErrors) {
          console.debug("error", response);
          // reject(response.errors[0]);
          const obj = {
            subscriptionID: "E521CD36-8931-4726-87DB-4F26962E7B76"
            // subscriptionID: response._httpResponse.body.subscriptions[0].subscriptionID
          };
          db.fetchSubscriptions(obj).then(() => {
            console.log("fetched", obj.subscriptionID);
            resolve();
          });
        } else {
          console.log("subscribed", response);
          resolve();
        }
      });
    });
  }

  static register(handler) {
    return new Promise(resolve => {
      const container = CloudKit.getDefaultContainer();
      if (container.isRegisteredForNotifications) resolve();
      container.addNotificationListener(handler);
      container.registerForNotifications().then(container => {
        if (container.isRegisteredForNotifications) console.debug("registered");
        resolve();
      });
    });
  }
}

export { AppleApi };
