import { AppleApi } from "./apple-api";

class AppleService {
  static subscribe(listId) {
    return AppleApi.subscribe(listId);
  }

  static register(handler) {
    return AppleApi.register(handler);
  }

  static createZone(zoneName) {
    return AppleApi.createZone(zoneName);
  }

  static createList(value) {
    return new Promise(resolve => {
      const record = {
        recordType: "List",
        fields: {
          title: {
            value: value
          }
        }
      };
      AppleApi.create(record).then(response => {
        const { recordName, fields } = response;
        resolve({
          id: recordName,
          title: fields.title.value
        });
      });
    });
  }

  static createItem(title, listId) {
    return new Promise(resolve => {
      const record = {
        recordType: "Item",
        fields: {
          title: {
            value: title
          },
          list: {
            value: {
              recordName: listId
            }
          }
        }
      };
      AppleApi.create(record).then(response => {
        const { recordName, recordChangeTag, fields } = response;
        resolve({
          id: recordName,
          title: fields.title.value,
          listId: fields.list.value.recordName,
          recordChangeTag: recordChangeTag
        });
      });
    });
  }

  // static getItem(id) {
  //   return new Promise((resolve) => {
  //     AppleApi.fetch(id)
  //       .then((response) => {
  //         const { recordName, recordChangeTag, fields } = response._results[0];
  //         const item = {
  //           id: recordName,
  //           title: fields.title.value,
  //           listId: fields.list.value.recordName,
  //           recordChangeTag: recordChangeTag
  //         };
  //         resolve(item);
  //       });
  //   });
  // }

  static updateItem(item, updates) {
    let fieldUpdates = {};
    Object.keys(updates).forEach(key => {
      fieldUpdates[key] = {
        value: updates[key]
      };
    });
    return new Promise(resolve => {
      const record = {
        recordType: "Item",
        recordName: item.id,
        recordChangeTag: item.recordChangeTag,
        fields: fieldUpdates
        // fields: {
        //   // list: {
        //   //   value: {
        //   //     recordName: item.listId,
        //   //     action: 'NONE'
        //   //   }
        //   // },
        //   title: {
        //     value: updates.title
        //   }
        // }
      };
      AppleApi.update(record).then(response => {
        resolve(response);
      });
    });
  }

  static updateList(list, updates) {
    let fieldUpdates = {};
    Object.keys(updates).forEach(key => {
      fieldUpdates[key] = {
        value: updates[key]
      };
    });
    return new Promise(resolve => {
      const record = {
        recordType: "List",
        recordName: list.id,
        recordChangeTag: list.recordChangeTag,
        fields: fieldUpdates
      };
      AppleApi.update(record).then(response => {
        resolve(response);
      });
    });
  }

  static shareList(list) {
    return new Promise(resolve => {
      const record = {
        recordType: "List",
        recordName: list.id
      };
      AppleApi.share(record).then(response => {
        resolve(response);
      });
    });
  }

  static acceptShare(shortGUID) {
    return AppleApi.acceptShare(shortGUID);
  }

  static removeItem(id) {
    return AppleApi.delete(id);
  }

  static removeList(id) {
    return AppleApi.delete(id);
  }

  static getLists() {
    return new Promise(resolve => {
      const query = {
        recordType: "List"
      };
      const options = {
        desiredKeys: ["title"],
        resultsLimit: 100
      };
      AppleApi.list(query, options).then(records => {
        resolve(
          records.map(record => {
            return {
              id: record.recordName,
              recordChangeTag: record.recordChangeTag,
              title: record.fields.title.value,
              modified: record.modified.timestamp
            };
          })
        );
      });
    });
  }

  static getItems(listId) {
    return new Promise(resolve => {
      const query = {
        recordType: "Item",
        filterBy: [
          {
            fieldName: "list",
            comparator: "EQUALS",
            fieldValue: {
              value: {
                recordName: listId
              }
            }
          }
        ]
      };
      const options = {
        desiredKeys: ["title", "list", "complete"],
        resultsLimit: 100
      };
      AppleApi.list(query, options).then(records => {
        resolve(
          records.map(record => {
            const item = {
              id: record.recordName,
              recordChangeTag: record.recordChangeTag,
              title: record.fields.title.value,
              listId: record.fields.list.value.recordName,
              modified: record.modified.timestamp
              // created: record.created.timestamp
            };
            // TODO gracefully handle new field
            if (record.fields.complete !== undefined) {
              item.complete = record.fields.complete.value;
            } else {
              item.complete = 0;
            }
            return item;
          })
        );
      });
    });
  }
}

export { AppleService };
