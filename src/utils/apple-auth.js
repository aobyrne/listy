import React from "react";

class AppleLogin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}
    };
    this.onApiLoaded = this.onApiLoaded.bind(this);
    this.configureApi = this.configureApi.bind(this);
    this.gotoAuthenticatedState = this.gotoAuthenticatedState.bind(this);
    this.gotoUnauthenticatedState = this.gotoUnauthenticatedState.bind(this);
    window.addEventListener("cloudkitloaded", this.onApiLoaded);
  }

  render() {
    return (
      <div>
        <div id="apple-sign-in-button" />
        <div id="apple-sign-out-button" />
      </div>
    );
  }

  configureApi() {
    const { token, container } = this.props;
    CloudKit.configure({
      locale: "en-us",
      containers: [
        {
          containerIdentifier: container,
          apiTokenAuth: {
            apiToken: token,
            persist: true,
            signInButton: {
              id: "apple-sign-in-button",
              theme: "black"
            },
            signOutButton: {
              id: "apple-sign-out-button",
              theme: "black"
            }
          },
          environment: "development"
        }
      ]
    });
  }

  onApiLoaded() {
    console.debug("onApiLoaded");
    this.configureApi();
    const container = CloudKit.getDefaultContainer();
    container.setUpAuth().then(userIdentity => {
      if (userIdentity) this.gotoAuthenticatedState(userIdentity);
      else this.gotoUnauthenticatedState();
    });
  }

  gotoAuthenticatedState(userIdentity) {
    const container = CloudKit.getDefaultContainer();
    const { nameComponents, userRecordName } = userIdentity;
    const { givenName, familyName } = nameComponents;
    this.props.onAuthSuccess({
      id: userRecordName,
      givenName,
      familyName
    });
    container.whenUserSignsOut().then(this.gotoUnauthenticatedState);
  }

  gotoUnauthenticatedState(error) {
    const container = CloudKit.getDefaultContainer();
    if (error && error.ckErrorCode === "AUTH_PERSIST_ERROR") {
      // showDialogForPersistError();
    }
    container
      .whenUserSignsIn()
      .then(this.gotoAuthenticatedState)
      .catch(this.gotoUnauthenticatedState);
  }
}

export { AppleLogin };
