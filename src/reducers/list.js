import {
  ADD_LIST_REQUEST,
  ADD_LIST_SUCCESS,
  ADD_LIST,
  GET_LISTS_REQUEST,
  REMOVE_LIST_REQUEST,
  REMOVE_LIST_SUCCESS,
  REMOVE_LIST,
  SELECT_LIST,
  SET_LISTS,
  TOGGLE_LISTS_EDITING,
  TOGGLE_LISTS,
  UPDATE_LIST_REQUEST,
  UPDATE_LIST_SUCCESS,
  UPDATE_LIST
} from "../actions";
import unionBy from "lodash/unionBy";

function lists(
  state = {
    isSyncing: false,
    toggle: true,
    editing: true,
    lists: []
  },
  { type, value }
) {
  switch (type) {
    case TOGGLE_LISTS: {
      const toggleValue = value === undefined ? !state.toggle : value;
      return Object.assign({}, state, {
        toggle: toggleValue
      });
    }
    case TOGGLE_LISTS_EDITING: {
      const toggleValue = value === undefined ? !state.editing : value;
      return Object.assign({}, state, {
        editing: toggleValue
      });
    }
    case ADD_LIST:
      return Object.assign({}, state, {
        isSyncing: true,
        lists: [value, ...state.lists]
      });
    case UPDATE_LIST:
      return Object.assign({}, state, {
        isSyncing: true,
        lists: state.lists.map(
          list =>
            list.id === value.id ? Object.assign({}, list, value.updates) : list
        )
      });
    case REMOVE_LIST:
      return Object.assign({}, state, {
        isSyncing: true,
        lists: state.lists.filter(list => list.id !== value)
      });
    case GET_LISTS_REQUEST:
    case ADD_LIST_REQUEST:
    case REMOVE_LIST_REQUEST:
    case UPDATE_LIST_REQUEST:
      return Object.assign({}, state, {
        isSyncing: true
      });
    case ADD_LIST_SUCCESS:
    case UPDATE_LIST_SUCCESS:
    case REMOVE_LIST_SUCCESS:
      return Object.assign({}, state, {
        isSyncing: false
      });
    case SET_LISTS:
      return Object.assign({}, state, {
        isSyncing: false,
        lists: unionBy(state.lists, value, "id")
      });
    default:
      return state;
  }
}

function selectedList(state = [], action) {
  const { type, value } = action;
  switch (type) {
    case SELECT_LIST:
      return value;
    default:
      return state;
  }
}

// export default combineReducers({
//   lists,
//   selectedList
// });

export { lists, selectedList };
