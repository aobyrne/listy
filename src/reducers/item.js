import {
  ADD_ITEM_REQUEST,
  ADD_ITEM_SUCCESS,
  ADD_ITEM,
  GET_ITEMS_REQUEST,
  REMOVE_ITEM_REQUEST,
  REMOVE_ITEM_SUCCESS,
  REMOVE_ITEM,
  SET_ITEMS,
  TOGGLE_ITEMS_EDITING,
  UPDATE_ITEM_REQUEST,
  UPDATE_ITEM_SUCCESS,
  UPDATE_ITEM
} from "../actions";
import unionBy from "lodash/unionBy";

function items(
  state = {
    isSyncing: false,
    editing: true,
    items: []
  },
  { type, value }
) {
  switch (type) {
    case TOGGLE_ITEMS_EDITING: {
      const toggleValue = value === undefined ? !state.editing : value;
      return Object.assign({}, state, {
        editing: toggleValue
      });
    }
    case ADD_ITEM:
      return Object.assign({}, state, {
        isSyncing: true,
        items: [value, ...state.items]
      });
    case UPDATE_ITEM:
      return Object.assign({}, state, {
        isSyncing: true,
        items: state.items.map(
          item =>
            item.id === value.id ? Object.assign({}, item, value.updates) : item
        )
      });
    case REMOVE_ITEM:
      return Object.assign({}, state, {
        isSyncing: true,
        items: state.items.filter(item => item.id !== value)
      });
    case GET_ITEMS_REQUEST:
    case ADD_ITEM_REQUEST:
    case UPDATE_ITEM_REQUEST:
    case REMOVE_ITEM_REQUEST:
      return Object.assign({}, state, {
        isSyncing: true
      });
    case ADD_ITEM_SUCCESS:
    case UPDATE_ITEM_SUCCESS:
    case REMOVE_ITEM_SUCCESS:
      return Object.assign({}, state, {
        isSyncing: false
      });
    case SET_ITEMS:
      return Object.assign({}, state, {
        isSyncing: false,
        items: unionBy(state.items, value, "id")
      });
    default:
      return state;
  }
}

// export function getVisibleItems(state) {
//   const { items, selectedList } = state;
//   return items.filter((item) => {
//     return item.listId === selectedList.id;
//   });
// }

// export default combineReducers({
//   items
// });

export { items };
