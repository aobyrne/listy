import { combineReducers } from "redux";
// import { default as lists } from './list';
// import { default as items } from './item';
import { lists, selectedList } from "./list";
import { items } from "./item";

export default combineReducers({
  lists,
  items,
  selectedList
});
