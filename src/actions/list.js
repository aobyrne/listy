export const GET_LISTS = "GET_LISTS";
export const GET_LISTS_REQUEST = "GET_LISTS_REQUEST";

export const SET_LISTS = "SET_LISTS";

export const ADD_LIST = "ADD_LIST";
export const ADD_LIST_REQUEST = "ADD_LIST_REQUEST";
export const ADD_LIST_SUCCESS = "ADD_LIST_SUCCESS";

export const UPDATE_LIST = "UPDATE_LIST";
export const UPDATE_LIST_REQUEST = "UPDATE_LIST_REQUEST";
export const UPDATE_LIST_SUCCESS = "UPDATE_LIST_SUCCESS";

export const REMOVE_LIST = "REMOVE_LIST";
export const REMOVE_LIST_REQUEST = "REMOVE_LIST_REQUEST";
export const REMOVE_LIST_SUCCESS = "REMOVE_LIST_SUCCESS";

export const SELECT_LIST = "SELECT_LIST";
export const SELECT_LIST_REQUEST = "SELECT_LIST_REQUEST";

export const TOGGLE_LISTS = "TOGGLE_LISTS";
export const TOGGLE_LISTS_EDITING = "TOGGLE_LISTS_EDITING";

export const SHARE_LIST_REQUEST = "SHARE_LIST_REQUEST";

export function getLists(value) {
  return { type: GET_LISTS, value };
}
export function getListsRequest(value) {
  return { type: GET_LISTS_REQUEST, value };
}

export function setLists(value) {
  return { type: SET_LISTS, value };
}

export function addList(value) {
  return { type: ADD_LIST, value };
}
export function addListRequest(value) {
  return { type: ADD_LIST_REQUEST, value };
}
export function addListSuccess() {
  return { type: ADD_LIST_SUCCESS };
}

export function updateList(value) {
  return { type: UPDATE_LIST, value };
}
export function updateListRequest(value) {
  return { type: UPDATE_LIST_REQUEST, value };
}
export function updateListSuccess() {
  return { type: UPDATE_LIST_SUCCESS };
}

export function toggleLists(value) {
  return { type: TOGGLE_LISTS, value };
}

export function toggleListsEditing(value) {
  return { type: TOGGLE_LISTS_EDITING, value };
}

export function removeList(value) {
  return { type: REMOVE_LIST, value };
}
export function removeListRequest(value) {
  return { type: REMOVE_LIST_REQUEST, value };
}
export function removeListSuccess() {
  return { type: REMOVE_LIST_SUCCESS };
}

export function selectList(value) {
  return { type: SELECT_LIST, value };
}
export function selectListRequest(value) {
  return { type: SELECT_LIST_REQUEST, value };
}

export function shareListRequest(value) {
  return { type: SHARE_LIST_REQUEST, value };
}
