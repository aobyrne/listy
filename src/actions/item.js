export const GET_ITEMS = "GET_ITEMS";
export const GET_ITEMS_REQUEST = "GET_ITEMS_REQUEST";

export const SET_ITEMS = "SET_ITEMS";

export const ADD_ITEM = "ADD_ITEM";
export const ADD_ITEM_REQUEST = "ADD_ITEM_REQUEST";
export const ADD_ITEM_SUCCESS = "ADD_ITEM_SUCCESS";

export const UPDATE_ITEM = "UPDATE_ITEM";
export const UPDATE_ITEM_REQUEST = "UPDATE_ITEM_REQUEST";
export const UPDATE_ITEM_SUCCESS = "UPDATE_ITEM_SUCCESS";

export const REMOVE_ITEM = "REMOVE_ITEM";
export const REMOVE_ITEM_REQUEST = "REMOVE_ITEM_REQUEST";
export const REMOVE_ITEM_SUCCESS = "REMOVE_ITEM_SUCCESS";

export const TOGGLE_ITEMS_EDITING = "TOGGLE_ITEMS_EDITING";

export function getItems(value) {
  return { type: GET_ITEMS, value };
}
export function getItemsRequest(value) {
  return { type: GET_ITEMS_REQUEST, value };
}

export function setItems(value) {
  return { type: SET_ITEMS, value };
}

export function addItem(value) {
  return { type: ADD_ITEM, value };
}
export function addItemRequest(value) {
  return { type: ADD_ITEM_REQUEST, value };
}
export function addItemSuccess() {
  return { type: ADD_ITEM_SUCCESS };
}

export function updateItem(value) {
  return { type: UPDATE_ITEM, value };
}
export function updateItemRequest(value) {
  return { type: UPDATE_ITEM_REQUEST, value };
}
export function updateItemSuccess() {
  return { type: UPDATE_ITEM_SUCCESS };
}

export function toggleItemsEditing(value) {
  return { type: TOGGLE_ITEMS_EDITING, value };
}

export function removeItem(value) {
  return { type: REMOVE_ITEM, value };
}
export function removeItemRequest(value) {
  return { type: REMOVE_ITEM_REQUEST, value };
}
export function removeItemSuccess() {
  return { type: REMOVE_ITEM_SUCCESS };
}
