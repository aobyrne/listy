import React from "react";
import { render } from "react-dom";
import { AppContainer } from "react-hot-loader";
import App from "components/app";
import configureStore from "store";
import rootSaga from "sagas";

const store = configureStore();
store.runSaga(rootSaga);
const el = document.getElementById("app");

render(
  <AppContainer>
    <App store={store} />
  </AppContainer>,
  el
);

if (module.hot) {
  module.hot.accept("components/app", () => {
    const DefaultApp = require("components/app").default;
    render(
      <AppContainer>
        <DefaultApp store={store} />
      </AppContainer>,
      el
    );
  });
}
