import React from "react";
import { connect } from "react-redux";
import * as actions from "actions";
import CSSModules from "react-css-modules";
import styles from "./style.css";
import Icon from "components/icon";

class List extends React.Component {
  constructor(props) {
    super(props);
    this._onClick = this._onClick.bind(this);
    this._onClickRemove = this._onClickRemove.bind(this);
  }

  render() {
    const { list, lists, active } = this.props;
    const { editing } = lists;
    let itemClass = "container";
    if (active) {
      itemClass += " active";
    }
    return (
      <div onClick={this._onClick} styleName={itemClass}>
        <span styleName="title">{list.title}</span>
        {editing && (
          <span onClick={this._onClickRemove}>
            <Icon type="remove" />
          </span>
        )}
      </div>
    );
  }

  _onClick() {
    const { list, selectList } = this.props;
    selectList(list);
  }

  _onClickRemove(e) {
    e.stopPropagation();
    const { list, removeList } = this.props;
    removeList(list.id);
  }
}

const mapStateToProps = ({ lists }) => {
  return {
    lists
  };
};

const mapActionsToProps = dispatch => {
  return {
    removeList: value => {
      dispatch(actions.removeListRequest(value));
    },
    selectList: value => {
      dispatch(actions.selectListRequest(value));
    },
    shareList: value => {
      dispatch(actions.shareListRequest(value));
    }
  };
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(CSSModules(List, styles, { allowMultiple: true }));
