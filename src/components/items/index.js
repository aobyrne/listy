import React from "react";
import { connect } from "react-redux";
import * as actions from "actions";
import Item from "../item";
import CSSModules from "react-css-modules";
import styles from "./style.css";
import Header from "components/header";
import TextInput from "components/text-input";
import Icon from "components/icon";

class Items extends React.Component {
  constructor(props) {
    super(props);
    this._addItem = this._addItem.bind(this);
    this._toggleEditing = this._toggleEditing.bind(this);
  }

  render() {
    const { items, lists, selectedList } = this.props;
    const { editing } = items;
    const editingClass = editing ? "editing" : "";
    const containerClass = `container ${editingClass}`;
    const hasActiveList = selectedList.hasOwnProperty("id");
    let listItems = items.items
      .filter(item => {
        return item.listId === selectedList.id;
      })
      .sort((a, b) => (a.modified > b.modified ? -1 : 1));
    const incompleteItems = listItems.filter(item => {
      return !item.complete;
    });
    const completeItems = listItems
      .filter(item => {
        return item.complete;
      })
      .sort((a, b) => (a.modified > b.modified ? -1 : 1));
    const visibleItems = incompleteItems.concat(completeItems);
    const plusIconStyle = {
      color: "#b5b5b5"
    };
    return (
      <div styleName={containerClass}>
        {hasActiveList && (
          <Header
            syncing={items.isSyncing || lists.isSyncing}
            menu={true}
            toggleEditing={this._toggleEditing}
            list={selectedList}
          >
            {selectedList.title}
          </Header>
        )}
        {editing && (
          <div styleName="inputContainer">
            <Icon type="plus" style={plusIconStyle} />
            <TextInput
              placeholder="Add item"
              onComplete={this._addItem}
              clearOnComplete
              className={styles.input}
            />
          </div>
        )}
        <div styleName="list">
          {visibleItems.map(item => (
            <Item key={item.id} item={item} />
          ))}
        </div>
        {!items.isSyncing &&
          !visibleItems.length && <div styleName="no-items">No items</div>}
      </div>
    );
  }

  _addItem(title) {
    const { addItem, selectedList } = this.props;
    const listId = selectedList.id;
    addItem(title, listId);
  }

  _toggleEditing() {
    this.props.toggleEditing();
  }
}

const mapStateToProps = ({ items, lists, selectedList }) => {
  return {
    items,
    lists,
    selectedList
  };
};

const mapActionsToProps = dispatch => {
  return {
    addItem: (title, listId) => {
      dispatch(actions.addItemRequest({ title, listId }));
    },
    selectList: list => {
      dispatch(actions.selectList(list));
    },
    toggleEditing: value => {
      dispatch(actions.toggleItemsEditing(value));
    }
  };
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(CSSModules(Items, styles, { allowMultiple: true }));
