import React from "react";
import CSSModules from "react-css-modules";
import styles from "./style.css";

function Spinner() {
  return (
    <div styleName="spinner-container">
      <div styleName="spinner" />
    </div>
  );
}

export default CSSModules(Spinner, styles);
