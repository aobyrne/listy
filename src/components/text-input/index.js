import React from "react";
import CSSModules from "react-css-modules";
import styles from "./style.css";

class TextInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: props.value
    };
    this._onChange = this._onChange.bind(this);
    this._onKeyDown = this._onKeyDown.bind(this);
  }

  componentDidMount() {
    this.input.focus();
  }

  render() {
    const { inputText } = this.state;
    const { className, placeholder } = this.props;
    return (
      <input
        ref={input => (this.input = input)}
        type="text"
        placeholder={placeholder}
        value={inputText}
        onKeyDown={this._onKeyDown}
        onChange={this._onChange}
        className={className}
      />
    );
  }

  _onKeyDown(event) {
    const { onComplete, clearOnComplete } = this.props;
    const value = event.target.value;
    if (event.keyCode === 13 && value) {
      onComplete(value);
      if (clearOnComplete) {
        this.setState({
          inputText: ""
        });
      }
    }
  }

  _onChange(event) {
    this.setState({
      inputText: event.target.value
    });
  }
}

export default CSSModules(TextInput, styles);
