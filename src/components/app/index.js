import React from "react";
import { Provider, connect } from "react-redux";
import * as actions from "actions";
import CSSModules from "react-css-modules";
import styles from "./style.css";
import Lists from "components/lists";
import Items from "components/items";
import { AppleLogin } from "utils/apple-auth";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}
    };
    this._onAuthSuccess = this._onAuthSuccess.bind(this);
  }

  render() {
    const { store, lists } = this.props;
    const { user } = this.state;
    return (
      <Provider store={store}>
        <div>
          {!user.id && (
            <div styleName="auth">
              <AppleLogin
                token="2bbeddb559ae409d6d5fbcbe663d3cb566f85cb5c6be0ecdeb677e70e0d92089"
                container="iCloud.com.listo"
                onAuthSuccess={this._onAuthSuccess}
              />
            </div>
          )}
          {user.id && (
            <div styleName="content">
              <div styleName={lists.toggle ? "lists active" : "lists"}>
                <Lists />
              </div>
              <div styleName={!lists.toggle ? "items active" : "items"}>
                <Items />
              </div>
            </div>
          )}
        </div>
      </Provider>
    );
  }

  _onAuthSuccess(userData) {
    console.debug("onAuthSuccess", userData);
    this.setState({
      user: userData
    });
    this.props.getLists();
    // .register((notification) => {
    //   const { selectedList } = this.props;
    //   const { _recordName, _reason } = notification;
    //   switch (_reason) {
    //     case 1:
    //       console.debug('create', _recordName);
    //       AppleService.getItem(_recordName)
    //         .then((item) => {
    //           if (item.listId === selectedList.id)
    //             addItem(item);
    //         });
    //       break;
    //     case 2:
    //       console.debug('update', _recordName);
    //       break;
    //     case 3:
    //       console.debug('delete', _recordName);
    //       removeItem(_recordName);
    //       break;
    //   }
    // })
  }
}

const mapStateToProps = state => {
  return {
    lists: state.lists
  };
};

const mapActionsToProps = dispatch => {
  return {
    getLists: () => {
      dispatch(actions.getListsRequest());
    }
  };
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(CSSModules(App, styles, { allowMultiple: true }));
