import React from "react";
import { connect } from "react-redux";
import * as actions from "actions";
import CSSModules from "react-css-modules";
import styles from "./style.css";
import TextInput from "components/text-input";
import Icon from "components/icon";

class Item extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editing: false
    };
    this._onClickTick = this._onClickTick.bind(this);
    this._onClickRemove = this._onClickRemove.bind(this);
    this._onEnterKey = this._onEnterKey.bind(this);
  }

  render() {
    const { title, complete, modified } = this.props.item;
    // const modifiedDate = (new Date(modified)).toISOString().split('T')[0];
    const { editing } = this.state;
    const editingClass = editing ? "editing" : "";
    const completeClass = complete === 1 ? "complete" : "";
    const containerClass = `container ${editingClass} ${completeClass}`;
    const tickIconStyle = {};
    const crossIconStyle = {};
    return (
      <div styleName={containerClass}>
        <span onClick={this._onClickTick}>
          <Icon type="tick" style={tickIconStyle} />
        </span>
        {!!complete && (
          <span onClick={this._onClickTitle} styleName="title">
            {title}
          </span>
        )}
        {!complete && (
          <TextInput
            value={title}
            onComplete={this._onEnterKey}
            placeholder=""
            className={styles.input}
          />
        )}
        {/* <span styleName="date">{modifiedDate}</span> */}
        <span onClick={this._onClickRemove}>
          <Icon type="remove" style={crossIconStyle} />
        </span>
      </div>
    );
  }

  _onClickTick() {
    const { item, updateItem } = this.props;
    const { complete } = item;
    const newVal = !complete ? 1 : 0;
    updateItem(item, { complete: newVal });
  }

  _onClickRemove() {
    const { item, removeItem } = this.props;
    removeItem(item.id);
  }

  _onEnterKey(title) {
    const { item, updateItem } = this.props;
    updateItem(item, { title: title });
  }
}

const mapStateToProps = ({ items }) => {
  return {
    items
  };
};

const mapActionsToProps = dispatch => {
  return {
    removeItem: id => {
      dispatch(actions.removeItemRequest(id));
    },
    updateItem: (item, updates) => {
      dispatch(actions.updateItemRequest({ item, updates }));
    }
  };
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(CSSModules(Item, styles, { allowMultiple: true }));
