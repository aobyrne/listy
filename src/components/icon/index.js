import React from "react";
import CSSModules from "react-css-modules";
import styles from "./style.css";

function Icon({ type, style }) {
  const iconClass = `icon icon-${type}`;
  return (
    <div styleName="icon-container">
      <div styleName={iconClass} style={style} />
    </div>
  );
}

export default CSSModules(Icon, styles, { allowMultiple: true });
