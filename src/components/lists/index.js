import React from "react";
import { connect } from "react-redux";
import * as actions from "actions";
import CSSModules from "react-css-modules";
import styles from "./style.css";
import Header from "components/header";
import List from "components/list";
import TextInput from "components/text-input";
import Icon from "components/icon";

class Lists extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: ""
    };
    this._addList = this._addList.bind(this);
    this._toggleEditing = this._toggleEditing.bind(this);
    this._onChange = this._onChange.bind(this);
  }

  render() {
    const { lists, selectedList } = this.props;
    const { editing } = lists;
    const editingClass = editing ? "editing" : "";
    const containerClass = `container ${editingClass}`;
    const plusIconStyle = {
      color: "#b5b5b5"
    };
    return (
      <div styleName={containerClass}>
        <Header syncing={lists.isSyncing} toggleEditing={this._toggleEditing}>
          Lists
        </Header>
        {editing && (
          <div styleName="inputContainer">
            <Icon type="plus" style={plusIconStyle} />
            <TextInput
              placeholder="Add list"
              onComplete={this._addList}
              clearOnComplete
              className={styles.input}
            />
          </div>
        )}
        {lists.lists.length > 0 && (
          <div styleName="list">
            {lists.lists
              .sort((a, b) => (a.title > b.title ? 1 : -1))
              .map(list => (
                <List
                  key={list.id}
                  list={list}
                  active={list.id === selectedList.id}
                />
              ))}
          </div>
        )}
      </div>
    );
  }

  _addList(title) {
    this.props.addList(title);
  }

  _toggleEditing() {
    this.props.toggleEditing();
  }

  _onChange() {
    this.setState({
      inputText: event.target.value
    });
  }
}

const mapStateToProps = state => {
  return {
    lists: state.lists,
    items: state.items,
    selectedList: state.selectedList
  };
};

const mapActionsToProps = dispatch => {
  return {
    addList: title => {
      dispatch(actions.addListRequest({ title }));
    },
    toggleEditing: value => {
      dispatch(actions.toggleListsEditing(value));
    }
  };
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(CSSModules(Lists, styles, { allowMultiple: true }));
