import React from "react";
import { connect } from "react-redux";
import * as actions from "actions";
import CSSModules from "react-css-modules";
import Spinner from "components/spinner";
import Icon from "components/icon";
import TextInput from "components/text-input";
import styles from "./style.css";

class Header extends React.Component {
  state = {
    editingTitle: false
  };

  render() {
    const { syncing, menu, settings } = this.props;
    return (
      <div styleName="container">
        <div
          styleName="title"
          onClick={() => {
            this.setState(
              { editingTitle: !this.state.editingTitle }
              // () => this.state.editingTitle && this.input.focus()
            );
          }}
        >
          {this.state.editingTitle && (
            <TextInput
              placeholder={this.props.list.title}
              onComplete={title => {
                this.props.renameList(this.props.list, { title });
                this.setState({ editingTitle: false });
              }}
              clearOnComplete
              className={styles.input}
            />
          )}
          {!this.state.editingTitle && this.props.children}
        </div>
        {settings && (
          <div onClick={this._toggleSettings} styleName="icon-left">
            <Icon type="settings" />
          </div>
        )}
        {menu && (
          <div onClick={this.props.toggleLists} styleName="icon-left icon-menu">
            <Icon type="menu" />
          </div>
        )}
        <div styleName="icon-right icon-spinner">{syncing && <Spinner />}</div>
      </div>
    );
  }
}

const mapActionsToProps = dispatch => {
  return {
    toggleLists: () => {
      dispatch(actions.toggleLists());
    },
    renameList: (list, updates) => {
      dispatch(actions.updateListRequest({ list, updates }));
    }
  };
};

export default connect(
  null,
  mapActionsToProps
)(CSSModules(Header, styles, { allowMultiple: true }));
